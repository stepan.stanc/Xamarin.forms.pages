using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Pages
{
  [XamlCompilation(XamlCompilationOptions.Compile)]
  public partial class Navig : ContentPage
  {
    public Navig()
    {
      InitializeComponent();

      pg1.Clicked += OnButton1Clicked;

      pg2.Clicked += OnButton2Clicked;

      pg3.Clicked += OnButton3Clicked;

    }
    private async void OnButton1Clicked(object sender, EventArgs e)
    {
      await Navigation.PushAsync( new NavigationPage(new ContentPage1()));
      //App.Current.MainPage = new NavigationPage(new ContentPage1());
    }
    private async void OnButton2Clicked(object sender, EventArgs e)
    {
      await Navigation.PushAsync(new NavigationPage(new ContentPage2()));
      //App.Current.MainPage = new NavigationPage(new ContentPage2());
    }
    private async void OnButton3Clicked(object sender, EventArgs e)
    {
      await Navigation.PushAsync(new NavigationPage(new ContentPage3()));
      //App.Current.MainPage = new NavigationPage(new ContentPage3());
    }

    protected override bool OnBackButtonPressed()
    {
      App.Current.MainPage = new MainPage();
      return true;
    }
  }
}
