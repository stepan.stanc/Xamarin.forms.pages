using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Pages
{
	public partial class MainPage : ContentPage
	{
		public MainPage()
		{
			InitializeComponent();
      pg1.Clicked += OnButton1Clicked;

      pg2.Clicked += OnButton2Clicked;

      pg3.Clicked += OnButton3Clicked;

      pg4.Clicked += OnButton4Clicked;

    }
    private void OnButton1Clicked(object sender, EventArgs e)
    {
      App.Current.MainPage = new Carousel();
    }
    private void OnButton2Clicked(object sender, EventArgs e)
    {
      App.Current.MainPage = new Tabbed();
    }
    private void OnButton3Clicked(object sender, EventArgs e)
    {
      App.Current.MainPage = new MasterDetail();
    }
    private async void OnButton4Clicked(object sender, EventArgs e)
    {
      //await Navigation.PushAsync(new NavigationPage(new Navig()));
      App.Current.MainPage = new NavigationPage(new Navig());
    }
  }
}
