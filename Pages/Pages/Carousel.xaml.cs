using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Carousel : CarouselPage
	{
		public Carousel ()
		{
			InitializeComponent ();
		}

    protected override bool OnBackButtonPressed()
    {
      App.Current.MainPage = new MainPage();
      return true;
    }
  }
}
