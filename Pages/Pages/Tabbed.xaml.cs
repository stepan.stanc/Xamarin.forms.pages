using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Pages
{
  [XamlCompilation(XamlCompilationOptions.Compile)]
  public partial class Tabbed : TabbedPage
  {
    public Tabbed()
    {
      InitializeComponent();
      /*
      Children.Add(new ContentPage1());
      Children.Add(new ContentPage2());
      Children.Add(new ContentPage3());
      */
    }
    protected override bool OnBackButtonPressed()
    {
      App.Current.MainPage = new MainPage();
      return true;
    }
  }  
}
